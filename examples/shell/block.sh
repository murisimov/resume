#!/bin/bash

PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# Attempts to cause warning state:
warning_limit=100
# Attempts to cause BAN!
danger_limit=200

danger=`tput setaf 1`
warning=`tput setaf 3`
info=`tput setaf 2`
end=`tput sgr0`

if [ ${#} -eq 0 ]; then
    if [ -f /var/log/secure ]; then
	log='/var/log/secure'
    elif [ -f /var/log/auth.log ]; then
	log='/var/log/auth.log'
    else
	echo "Secure log ${danger}not${end} found, please enter it as first argument"
	exit 1
    fi
elif [ ${#} -gt 1 ]; then
    echo "${danger}Too many${end} arguments. There should be only one log file."
    exit 1
elif ! [ -f ${1} ]; then
    echo "This is ${danger}not${end} valid log file, please enter full path to secure log file."
    exit 1
else
    log=${1}
fi

grep 'Failed password for invalid' ${log}|awk '{print $13}'|sort -n|uniq -c|sort -nr|head -10|while read line; do
    attempts=${line% *}
    ip=${line#* }
    if [ ${attempts} -le ${warning_limit} ]; then
	color=${info}
	echo -e "${color}${attempts}${end} ${ip}"
    elif [ ${attempts} -gt ${danger_limit} ]; then
	color=${danger}
	if [ -n "`iptables -L INPUT -v -n|grep ${ip}`" ]; then
	    echo -e "${color}${attempts}${end} ${ip}, ${info}already blocked${end}."
	else
	    iptables -A INPUT -s ${ip} -j DROP
	    echo -e "${color}${attempts}${end} ${ip}, ${warning}just blocked${end}."
	fi
    else
	color=${warning}
	echo -e "${color}${attempts}${end} ${ip}"
    fi
done
exit 0
