```python
filetype = "resume"

name     = "Andrii"
lastname = "Murisimov"

assessments = {
    1: "I tried to poke this thing but did not go further",

    2: "I know the most common use cases and I can handle it if necessary",

    3: "I know how to use it, but there's still a lot to learn",

    4: "I have often used it and familiar with it quite well",

    5: "I have no such skills yet"
}
```

---
Administration
---
- **OS**
    - <img src="https://www.centos.org/favicon.ico" width="16" height="16"> CentOS
    - <img src="https://cdn1.iconfinder.com/data/icons/system-shade-circles/512/debian-128.png" width="16" height="16"> Debian

- [GNU Coreutils](https://en.wikipedia.org/wiki/GNU_Core_Utilities) / [BusyBox](https://en.wikipedia.org/wiki/BusyBox): `3`

- **Monitoring**
    - <img src="https://support.zabbix.com/s/en_US-mrny05/6331/6/_/favicon.ico" width="16" height="16"> [Zabbix](http://www.zabbix.com/): `3`

- **Automation**
    - <img src="https://www.ansible.com/hs-fs/hub/330046/file-448313641-png/favicon.png?t=1489163439052" width="16" height="16"> [Ansible](https://www.ansible.com/): `2`

- **DBMS**
    - <img src="https://www.mongodb.com/assets/images/global/favicon.ico" width="16" height="16"> [MongoDB](https://www.mongodb.com/): `4`
    - <img src="https://redis.io/images/favicon.png" width="16" height="16"> [Redis](https://redis.io/): `3`
    - <img src="https://cdn1.iconfinder.com/data/icons/simple-icons/128/mysql-128-black.png" width="16" height="16"> [MySQL](https://www.mysql.com/): `2`
    - <img src="https://cdn-img.easyicon.net/png/5410/541006.gif" width="16" height="16"> [PostgreSQL](https://www.postgresql.org/): `2`

- **Packaging**
    - .rpm (rpmbuild / createrepo):   `3`
    - .deb (dpkg / reprepro / gdebi) `3`
    - python packaging: `4`
        - [virtualenv](https://virtualenv.pypa.io/en/stable/)
        - [setuptools](https://setuptools.readthedocs.io/en/latest/)
        - [pip](https://pip.pypa.io/en/stable/)
    - javascript packaging: `2`
        - [npm](https://www.npmjs.com/)
        - [bower](https://bower.io/)

- **Webservers**
    - <img src="https://pbs.twimg.com/profile_images/567774844322713600/tYoVju31.png" width="16" height="16"> [Nginx](https://www.nginx.com/resources/wiki/):   `3`
    - <img src="http://agurlek.com/tr/media/blob/apache-feather.ico" width="16" height="16"> [Apache2](https://httpd.apache.org/): `3`

- **CI**
    - <img src="http://www.yolinux.com/TUTORIALS/images/Jenkins/Jenkins_logo.png" width="16" height="16"> [Jenkins](https://jenkins.io/):   `1`
    - <img src="https://gitlab.com/assets/favicon-075eba76312e8421991a0c1f89a89ee81678bcde72319dd3e8047e2a47cd3a42.ico" width="16" height="16"> [Gitlab CI](https://about.gitlab.com/gitlab-ci/): `1`

---
Development
---
- **Languages**
    - <img src="https://chocolatey.org/content/packageimages/python.3.6.0.svg" width="16" height="16">\ Python:     `4`
    - <img src="https://cdn2.iconfinder.com/data/icons/designer-skills/128/code-programming-javascript-software-develop-command-language-128.png" width="16" height="16"> Javascript: `3`
    - <img src="http://icons.iconarchive.com/icons/blackvariant/button-ui-system-apps/128/Terminal-icon.png" width="16" height="16"> Shell:      `2`

- **Frameworks**
    - Python
        - <img src="http://www.tornadoweb.org/en/stable/_images/tornado.png" width="16" height="16"> [Tornado](http://www.tornadoweb.org/en/stable/): `4` ([example](https://gitlab.com/murisimov/wormhole_tracker))
        - <img src="http://shere.buruma.net/wp-content/uploads/2014/09/django.png" width="16" height="16"> [Django](https://www.djangoproject.com/):  `1`
        - <img src="http://flask.pocoo.org/docs/0.12/_static/flask.png" width="16" height="16"> [Flask](http://flask.pocoo.org/):   `1` ([example](https://gitlab.com/murisimov/htpass))

    - Javascript
        - <img src="https://material.angularjs.org/latest/favicon.ico" width="16" height="16"> [AngularJS](https://angularjs.org/): `3`
        - <img src="https://plugins.jquery.com/jquery-wp-content/themes/plugins.jquery.com/i/favicon.ico" width="16" height="16"> [jQuery](https://jquery.com/):    `2`
        - <img src="https://facebook.github.io/react/favicon.ico" width="16" height="16"> [ReactJS](https://facebook.github.io/react/):   `1`

- **General**
    - <img src="https://alligator.io/images/logos/html5-logo.svg" width="16" height="16"> HTML:  `3`
    - <img src="https://webref.ru/assets/images/book/css.png" width="16" height="16"> CSS:   `3`
    - [Regex](http://www.regular-expressions.info/): `3`
    - REST
    - Data visualization
        - Google Charts
        - Flot.js
        - Treant.js

- **Collaboration**
    - <img src="https://git-scm.com/favicon.ico" width="16" height="16"> [Git](https://git-scm.com/):     `3`
    - <img src="https://pbs.twimg.com/profile_images/552177275911671808/JiszgZdZ.png" width="16" height="16"> [Trello](https://trello.com/): `3`
    - <img src="https://slack.com/img/slack_hash_128.v1464126652.png" width="16" height="16"> [Slack](https://slack.com/)   `4`
